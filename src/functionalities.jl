
""" function get_expectation_value(state_operator)
    returns the expectation value <state|opertor|state>
"""
function get_expectation_value(state, operator)
    return dot(state, operator, state)
end


""" function get_variance(state_operator)
    returns the get_variance
"""
function get_variance(state, operator)
    return sqrt(dot(state, operator*operator, state)- dot(state, operator, state)^2)
 end


"""function measure_gs(H, operator)
calculate (possibly degenerated) the ground state of Hamiltonian H, returns
expectation value of operator given
"""
function measure_GS(H, operator)
    state = get_GS(H)
    measure = transpose(state)*operator*state
    return measure
end

""" function get_GS
   returns ground state of the Hamiltonian H
   if the ground state is degenerate, returns superposition of all the ground states
    #ToDo: find other way to deal with this superposition
"""
function get_GS(H)
    eig_vecs = eigvecs(H)
    eig_vals = eigvals(H)
    state = eig_vecs[1:end, 1]
    number = 1
    for i in 2:length(eig_vals)
        if abs(eig_vals[1]-eig_vals[i]) < 1e-8 #for degenerate ground states we have to sum over the different combinations
            state += eig_vecs[1:end,i]
            number += 1
        end
    end
    state /= norm(state)
    return state
end


""" measure_spin_spin_correlator(i,j,H, op)
    calculates spin-spin correlator of the ground state (degeneracy included)
    of Hamiltonian H at sites i,j. For definition and properties see Lidia's 
    master thesis
"""
function measure_spin_spin_correlator(i,j,state, op)
    N_up1 = op.N_up(i)
    N_dn1 = op.N_dn(i)
    N_up2 = op.N_up(j)
    N_dn2 = op.N_dn(j)
    spin_spin_corr = -2.0*((dot(state, N_up1*N_dn2, state) - get_expectation_value(state, N_up1)*get_expectation_value(state, N_dn2)) + (dot(state, N_dn1*N_up2, state) - get_expectation_value(state, N_dn1)*get_expectation_value(state, N_up2)))
    return spin_spin_corr
end