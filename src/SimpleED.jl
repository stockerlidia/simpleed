module SimpleED
    using LinearAlgebra
    include("electronic.jl")
    include("fermionic.jl")
    include("functionalities.jl")

    include("exports.jl")
end # module
